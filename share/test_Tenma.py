#!/usr/bin/env python
import os
import sys
import argparse
from time import sleep
from TenmaPSU import Tenma



parser=argparse.ArgumentParser()
parser.add_argument('-p','--port',help="port",default="/dev/ttyUSB2")
parser.add_argument('-v','--v',help="verbose",action="store_true")
parser.add_argument('-volt','--volt',help="volt .2f [V]",type=float,default=1.)
parser.add_argument('-amp','--amp',help="current .3f [A]",type=float,default=0.001)
args=parser.parse_args()

port=args.port
verbose=args.v
volt=args.volt
amp=args.amp

if verbose:
    print("port:\t%s"%(port))
    print("volt:\t%.2f"%(volt))
    print("amp:\t%.2f"%(amp))
pass

psu =Tenma(port)

print("setting the version")
version = psu.getVersion()
print(version)
s=psu.getStatus()
print(s)
print("setting the V")
psu.setVoltage(volt)
V = psu.getSetVoltage()
print(V)
outV=psu.getVoltage()
print(outV)

sleep(0.5)


print("setting the A")
psu.setCurrent(amp)
A = psu.getSetCurrent()
print(A)
outA=psu.getCurrent()
print(outA)
psu.enableOutput(True)
psu.getStatus()

##psu.rampVoltage(30,5,2)
