#!/usr/bin/env python
# florian.haslbeck@cern.ch

import sys
import argparse
from ThorlabsKDC101 import ThorlabsKDC101 

def get_formated_position(axis, isSteps):
    """ get a string with the position and the unit """
    return "%s %s"%(axis.getPos(inSteps=isSteps),'steps' if isSteps else 'deg')

parser = argparse.ArgumentParser("Terminal control for the Thorlabs Kinesis 101 KDC Rotational stage")
parser.add_argument('-p','--port', type=str, help="Port", default='/dev/serial/by-id/usb-Thorlabs_Brushed_Motor_Controller_27261650-if00-port0')
parser.add_argument('-d','--degree', type=float, default=0, help="The degree to move. Or steps if -steps.")
parser.add_argument('-m','--mode', type=str, default='abs' , choices=['abs' , 'rel'], help="Mode: Move (abs)olute or (rel)ative")
parser.add_argument('-steps','--steps',  action='store_true', help="Use this command to use steps instead of degrees.")
parser.add_argument('-stop','--stop',action='store_true',help='Stop the axis.')
parser.add_argument('-i','--info',action='store_true',help='Only print out current position.')
args = parser.parse_args()


axis = ThorlabsKDC101(port = args.port)

# stop the axis
if args.stop: axis.stop(); print("Stopped at %s"%axis.getPos(inSteps=args.steps)); sys.exit(1)

# inform about current position
if args.info: 
    print("Axis is currently at position: %s %s"%(axis.getPos(inSteps=args.steps), 'steps' if args.steps else 'deg'))
    sys.exit(1)

# before anything else, wait until axis stops
if axis.isMoving(): print("Axis is currently moving. Please wait for command to finish. Exiting.\n");sys.exit(1)

degree , isAbs, isSteps = args.degree, args.mode == "abs", args.steps
print("%s mode selected."%('Absolut' if isAbs else 'Relative'))
print("%s mode selected."%('Steps' if isSteps else 'Degrees'))
  

print("Axis is currently at position: %s %s"%(axis.getPos(inSteps=isSteps), 'steps' if isSteps else 'deg'))
axis.moveAbs(args.degree,inSteps=isSteps) if isAbs else axis.moveRel(args.degree,inSteps=isSteps)

axis.waitForStop() 

while axis.getPos(inSteps=isSteps) == -978306.619: pass # bug fix sometimes reading fails
print("Axis stopped. New position reads %s %s"%(axis.getPos(inSteps=isSteps), 'steps' if isSteps else 'deg'))

print("Goodbye.")
