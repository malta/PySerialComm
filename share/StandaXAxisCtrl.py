#!/usr/bin/env python
import sys
import argparse
from time import sleep
from StandaAxis import StandaAxis

# Base unit is mm, hence, we can simply multiply by a um, mm, or cm to get the proper units. 
CM = 10.   
MM = 1.
UM = 0.001 

min_pos = -10*CM
max_pos = 1.5*CM

parser = argparse.ArgumentParser()
parser.add_argument('-m','--mode', type=str, required=True, choices = ['abs' , 'rel'], help="Mode: Move (abs)olute or (rel)ative")
parser.add_argument('-d','--distance', type=float, required=True, help="The distance to move")
parser.add_argument('-u','--unit',  type=str, required=True, help="Unit for the distance: um, mm, or cm")
parser.add_argument('-v','--verbose',action='store_true')
args = parser.parse_args()
distance = args.distance

isAbs = ( args.mode == "abs" )
print("%s mode selected\n"%('Absolut' if isAbs else 'Relative'))
  
if   (args.unit == "um") : args.distance = args.distance * UM
elif (args.unit == "mm") : args.distance = args.distance * MM
elif (args.unit == "cm") : args.distance = args.distance * CM
else :
    print("Unknown units. Exiting.\n")
    sys.exit(1)
    
axis = StandaAxis(port =  "/dev/serial/by-id/usb-XIMC_XIMC_Motor_Controller_000046C6-if00", micronPerStep = 2.5, verbose = args.verbose)
  
if args.verbose: print("Get the status")
if not axis.getStatus(): print ("Problem querying axis. Exiting."); sys.exit(1)
if axis.moving: print("Axis is currently moving. Please wait for command to finish. Exiting.\n");sys.exit(1)

print("Axis currently at position: %.3f mm"% axis.pos)
new_pos = args.distance if isAbs else args.distance+axis.pos
if new_pos<min_pos or new_pos>max_pos: print("New position: %.6f mm outside of allowed range. Exiting.\n"% new_pos); sys.exit(1)

print("All checks passed. Will move to position: %.3f mm\n"% new_pos)

ok = axis.moveAbs(args.distance) if isAbs else axis.moveRel(args.distance)

if ok: print("Moving...")
else: print("could not move... Exiting"); sys.exit(1)

while True:
    if not axis.getStatus(): print ("Problem querying axis. Exiting."); sys.exit(1)
    if axis.moving: print("Axis still moving ... current position %.3fmm" % axis.pos)
    else: print("Axis stopped at position %.3fmm" % axis.pos); break
    sleep(1)
