In order to overcome the problem of random port assignments when using a USB-4xRS232 or 8xRS232 device, follow the below instructions:

1. Connect the device via USB to the PC.

2. In a terminal, list the symlinks automatically assigned to each port with the following command: ll /dev/serial/by-id/
If the USB to RS232 device is properly connected and powered, you will see output similar to: 

lrwxrwxrwx. 1 root root 13 Aug 26 11:03 usb-Digilent_Digilent_Adept_USB_Device_210203A25710-if01-port0 -> ../../ttyUSB5
lrwxrwxrwx. 1 root root 13 Aug 26 10:58 usb-FTDI_USB__-__Serial_Cable_FT1T6HRA-if00-port0 -> ../../ttyUSB0
lrwxrwxrwx. 1 root root 13 Aug 26 10:58 usb-FTDI_USB__-__Serial_Cable_FT1T6HRA-if01-port0 -> ../../ttyUSB1
lrwxrwxrwx. 1 root root 13 Aug 26 10:58 usb-FTDI_USB__-__Serial_Cable_FT1T6HRA-if02-port0 -> ../../ttyUSB2
lrwxrwxrwx. 1 root root 13 Aug 26 10:58 usb-FTDI_USB__-__Serial_Cable_FT1T6HRA-if03-port0 -> ../../ttyUSB3

3. Using the mapping provided in this output, use the relevant symlink name in the following format when defining a port in,
for instance a python/C++ script:

"/dev/serial/by-id/usb-FTDI_USB__-__Serial_Cable_FT1T6HRA-if00-port0"

instead of:

"/dev/ttyUSB0"

The symlink will always dynamically point to the correct USB port name, irrespective of cable connection order or rebooting the pc.