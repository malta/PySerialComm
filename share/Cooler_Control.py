#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 11 12:12:30 2021

@author: Vlad B.
"""

from Julabo import Julabo
import threading
import time
import argparse


parser=argparse.ArgumentParser()
parser.add_argument("-p","--port",default="/dev/serial/by-id/usb-FTDI_USB__-__Serial_Cable_FT5QI4J3-if00-port0") # "/dev/serial/by-id/usb-FTDI_USB__-__Serial_Cable_FT5QI4J3-if00-port0"
parser.add_argument("-s","--set",help="Set_temp_value",type=int,required=True)
parser.add_argument("-l","--llim",help="Lower limit for temperature",type=float,required=True)
parser.add_argument("-u","--ulim",help="Upper limit for temperature",type=float,required=True)
args=parser.parse_args()

print ("\nTest agilent")
print ("Port: %s"     % args.port)
print ("Set_Value: %i"     % args.set)
print ("Bot_limit: %.2f"   % args.llim)
print ("Top_limit: %.2f"   % args.ulim)


j =Julabo(portname = args.port)

def get_telemetry():
    while True:
        
        stat = j.getStatus()
        set_pt = j.getSetPoint()
        temp = j.getTemperature()
        temp_lmt = j.getTempLimit()
        time.sleep(5)
        
        print("Status: ",stat," ---"," Set point: ",set_pt," ---"," Current temperature: ",temp," ---"," Temperature limits: ",temp_lmt)
"""        
def set_SetPoint():
    try:
        set_point = j.toFloat(input("Insert Set Point: "))
        j.setSetPoint(set_point)
        print('\n',"Set point updated to: ",j.getSetPoint())
        time.sleep(1)
    except: 
        print("No modification on the set point")
        time.sleep(1)
        pass
"""
def set_TempLimit():
    if args.set != "skip":
    
    
        set_point = j.toFloat(args.set)
        j.setSetPoint(set_point)
        set_botlmt = j.toFloat(args.llim)
        toplmt = j.toFloat(args.ulim)
        j.setTempLimit(set_botlmt,toplmt)
        print("Temperature Limits updated to: ",j.getTempLimit())
        time.sleep(1)
    else:
        pass
   

if __name__ == "__main__": 
	# create threads
    t1 = threading.Thread(target=get_telemetry) 
    #t2 = threading.Thread(target = set_SetPoint)
    t3 = threading.Thread(target = set_TempLimit)
 
    #t2.start()
    t3.start()
    #t2.join()
    t3.join()

    t1.start() 
	#set_SetPoint() 
	#set_TempLimit()


  
        

