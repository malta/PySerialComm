#################
# Lingxin Meng  #
# lmeng@cern.ch #
#################

import sys
sys.path.append("../python/")
import signal
import time, datetime
import matplotlib
import matplotlib.pyplot as plt
matplotlib.use('TkAgg')
import numpy
import logging, coloredlogs, verboselogs
import cts_t65


def loop(target,dwelltime): ## dwelltime in minutes
	# dwelltime = dwelltime*60 ## dwelltime in seconds
	dwelltime = dwelltime ## dwelltime in seconds
	settemp = cc.getSetTemperature()
	loopstart = 0
	rampstart = 0
	rampend = 0
	ramprate = 0
	soakstart = 0
	soakend = 0
	
	if not settemp == target:
		logger.notice("Set temperature target from "+str(settemp)+" to "+str(cc.setTemperature(target)))
		settemp = cc.getSetTemperature()
		loopstart = time.time()
		
	starttemp = cc.getTemperature()
	temp = starttemp
	tempdiff = settemp - starttemp ## > 0 if warming up, < 0 if cooling down
	
	while not (temp > target-tolerance and temp < target+tolerance):
		temp = cc.getTemperature()
		logger.spam(str(sys._getframe(  ).f_code.co_name)+" current "+str(temp)+", set "+str(settemp)+", target "+str(target))
		### differ cooling and heating
		if rampstart == 0:
			if (tempdiff < 0 and temp <= starttemp + 0.1*tempdiff) or (tempdiff > 0 and temp >= starttemp + 0.1*tempdiff):
				logger.debug("Ramp start "+str(rampstart))
				rampstart = time.time()
				logger.debug( "Ramp start at "+datetime.datetime.fromtimestamp(rampstart).strftime(timeformat) )
		if rampend == 0:
			if (tempdiff < 0 and temp <= starttemp + 0.9*tempdiff) or (tempdiff > 0 and temp >= starttemp + 0.9*tempdiff):
				logger.debug(rampend)
				rampend = time.time()
				logger.debug( "Ramp end at "+datetime.datetime.fromtimestamp(rampend).strftime(timeformat) )
		updateplot(time.time()-starttime,temp)
		time.sleep(5)
	
	if rampstart == 0:
		rampstart = loopstart
	if rampend == 0:
		rampend = time.time()
	
	ramptime = (rampend-rampstart)/60. ## minutes
	try:
		ramprate = tempdiff/ramptime
	except Exception as e:
		logger.error(e)
		logger.debug( "Ramp start at "+datetime.datetime.fromtimestamp(rampstart).strftime(timeformat) )
		logger.debug( "Ramp end at "+datetime.datetime.fromtimestamp(rampend).strftime(timeformat) )
	logger.info("Ramp from "+str(starttemp)+" to "+str(target)+" in "+str(ramptime)+" minutes.")
	logger.info("Ramp rate = "+str(ramprate)+"/minute")

	if temp > target-tolerance and temp < target+tolerance:
		soakstart = time.time()
	
	while not time.time() > soakstart+dwelltime:
		logger.spam(str(sys._getframe(  ).f_code.co_name)+" soaking at "+str(temp)+", set "+str(settemp)+", target "+str(target))
		updateplot(time.time()-starttime,temp)
		time.sleep(5)

def stopandcleanup():
	if plt.fignum_exists(plt.gcf().number):# and args.save:
		logger.info("Saving plot")
		plt.savefig("thermalcycle"+datetime.datetime.fromtimestamp(time.time()).strftime(timeformatshort)+".png")	
	logger.info("Stopping at "+str(cc.getTemperature()))
	cc.stop()
	

def signal_handler(sig, frame):
	logger.debug(sig)
	stopandcleanup()
	exit()
	
def updateplot(xval, yval):
	xdata = numpy.append(serie.get_xdata(), xval)
	ydata = numpy.append(serie.get_ydata(), yval)
	# logger.spam("xdata "+str(xdata)+" ydata "+str(ydata))
	serie.set_data(xdata, ydata)
	ax.relim()
	ax.autoscale_view()
	fig.canvas.draw()
	fig.canvas.flush_events()
	plt.pause(0.001)


timeformat = "%Y-%m-%d %H:%M:%S"
timeformatshort = "%Y%m%d%H%M"

if __name__ == "__main__":
	signal.signal(signal.SIGTERM, signal_handler)
	signal.signal(signal.SIGINT, signal_handler)

	# waittime = 5*60 ### seconds
	waittime = 10 ### seconds
	starttime = 0

	coloredlogs.install(level="SPAM") ### controls availability of colored logs, set to lowest level
	logger = verboselogs.VerboseLogger('climatechamber')
	logger.setLevel(logging.SPAM) ### set this to control level of #logger

	### Test logger level
	logger.spam("SPAM")
	logger.debug("DEBUG")
	logger.info("INFO")
	logger.notice("NOTICE")
	logger.warning("WARNING")

	cc = cts_t65.CTS_T65("/dev/serial/by-id/usb-FTDI_FT232R_USB_UART_A907R8NF-if00-port0")
	tolerance = 1. #degrees

	if cc.isOn():
		logger.error("Climate chamber already running! Current temperature is "+str(cc.getTemperature()))
		cc.stop()
		exit()

	cc.setTemperature(20)
	cc.start()
	starttime = time.time()

	temp = cc.getTemperature()

	fig, ax = plt.subplots()
	# plt.tight_layout(pad=2, w_pad=0, h_pad=0)
	# plt.subplots_adjust(hspace=0)
	serie, = ax.plot([0], [temp], "-o")
	ax.set_autoscalex_on(True)
	ax.set_autoscaley_on(True)
	ax.set_xlabel("Time (s)")
	ax.set_ylabel("Temperature ($^\circ$C)")
	ax.grid()
	
	fig.canvas.draw()
	fig.canvas.flush_events()
	plt.pause(0.0001)


	while time.time() < starttime + waittime:
		temp = cc.getTemperature()
		logger.spam(temp)
		updateplot(time.time()-starttime,temp)
		time.sleep(5)
		
	for i in range(2):
		logger.info("--- Cycle "+str(i))
		# loop(-45,15)
		# loop(40,15)
		loop(5,15)
		# loop(30,15)
		
	# loop(-55,15)
	# loop(60,15)
	
	loop(20,10)
	
	stopandcleanup()
	exit(0)
