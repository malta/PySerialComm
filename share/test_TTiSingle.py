#!/usr/bin/env python

import TTiSingle
import time

ps = TTiSingle.TTiSingle('/dev/ttyUSB0')

print ps.getVersion()

ps.setVerbose(True)
ps.setVoltage(1)
ps.setCurrent(0.0)
ps.enableOutput(True)

print(ps.getVoltage())

time.sleep(5)

ps.enableOutput(False)
raw_input('wait')
