#!/usr/bin/env python
##################################################################
# This is a test script (binary) for the Standa Linear Stages
# Carlos.Solans@cern.ch
# December 2019
# July 2021 - add more parameters
##################################################################

import os
import sys
import argparse
import StandaAxis

parser=argparse.ArgumentParser('Test a Standa linear stage')
parser.add_argument('-p','--port',help="USB port (/dev/ttyACM1)", required=True)
parser.add_argument('-m','--move',type=float,help="move to position")
parser.add_argument('--home',help="go home",action="store_true")
parser.add_argument('-v','--verbose',help="verbose",action="store_true")
args=parser.parse_args()

print("Create new StandaAxis port:%s" % (args.axis, args.port))
ax=StandaAxis.StandaAxis(args.port)
ax.setVerbose(args.verbose)

print("Position: %i" % ax.getPos())

if args.home:
    print("Going home")
    ax.goHome()

if args.move:
    print("Move to position: %i" % args.move)
    ax.setPos(args.move)
    ax.waitForStop()
    ax.getPos()

print("Have a nice day")




