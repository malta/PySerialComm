#!/usr/bin/env python
import datetime
import ROOT
import os
import array
import time
import Newport
import time
import argparse

parser=argparse.ArgumentParser()
parser.add_argument("-p","--port",default="/dev/ttyUSB0")
parser.add_argument("-X","--MaxX",type=float,default=75)
parser.add_argument("-Y","--MaxY",type=float,default=75)
parser.add_argument("-x","--x_step",type=float,default=0.1)
parser.add_argument("-y","--y_step",type=float,default=0.1)
parser.add_argument("-c","--center",type=float,nargs=3,default=[-46.7622,11.1571,4.7])
parser.add_argument("-v","--verbose",action="store_true")
args=parser.parse_args()

motor = Newport.Newport(args.port)
motor.sc.setVerbose(args.verbose)

print "Enable motors"
motor.enableAllMotors()
print motor.getError()
motor.setVelocity(1,1)
print motor.getError()
motor.setVelocity(2,1)
print motor.getError()
motor.setVelocity(3,1)
print motor.getError()

for i in xrange(3):
    print "Move %i" % (i+1)
    motor.moveAbsolute(i+1,args.center[i]) #CHECK VALUES
    print motor.getError()
    motor.waitForStop(i+1)
pass
print "Position X=",motor.getCurrentPosition(1)
print "Position Y=",motor.getCurrentPosition(2)
print "Position Z=",motor.getCurrentPosition(3)

motor.close()

print "Have a nice day"
