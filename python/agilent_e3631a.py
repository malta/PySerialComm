#!/usr/bin/env python
#############################################
# Agilent_E3631A power supply control
#
# Carlos.Solans@cern.ch
# Abhishek.Sharma@cern.ch
# zainulabedin.akuji@cern.ch
# July 2018
#############################################

from time import sleep
from SerialCom import SerialCom

class agilent_e3631a:
    sc = None
    def __init__(self,portname):
        self.sc = SerialCom(portname,baudrate = 9600)

    def getModel(self):
        return self.sc.writeAndRead("*IDN?")

    def enableRemote(self):
        self.sc.write("SYST:REM")

    def setOutput(self,iOutput):
        self.sc.write("INST:NSEL %i"%(iOutput))

    def setVoltage(self,fValue):
        self.sc.write("VOLT %f"%(fValue))

    def rampVoltage(self,startV,endV,steps,delay):
        Vstep = (endV*1.-startV*1.)/steps
        for i in range(steps+1):
            self.setVoltage(startV + i*Vstep)
            sleep(delay)

    def setCurrentLimit(self,fValue):
        self.sc.write("CURR %f"%(fValue))

    def getVoltage(self):
        return float(self.sc.writeAndRead("VOLT?"))

    def getVoltageLimit(self):
        return float(self.sc.writeAndRead("SOUR:VOLT?"))

    def getCurrentLimit(self):
        return float(self.sc.writeAndRead("SOUR:CURR?"))

    def getSetVoltage(self):
        return float(self.sc.writeAndRead("SOUR:VOLT?"))

    def getSetCurrent(self):
        return float(self.sc.writeAndRead("SOUR:CURR?"))

    def getCurrent(self):
        self.sc.writeAndRead("CURR?")
        self.sc.writeAndRead("CURR?")
        return float(self.sc.writeAndRead("CURR?"))

    def enableOutput(self,bValue):
        if bValue == True:
            self.sc.write("OUTP ON")
            pass
        elif bValue == False:
            self.sc.write("OUTP OFF")
            pass
        pass
    
    def isEnabled(self):
        return self.sc.writeAndRead("OUTP?")=="1"

    def systemError(self):
        return self.sc.writeAndRead("SYST:ERR?")

    def reset(self):
        return self.sc.writeAndRead("*RST")

    def close(self):
        self.sc.close()





