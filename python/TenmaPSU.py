#!/usr/bin/env python
#############################################
# Tenma power supply control
# Model 72-2795, 30V, 3A
# Florian.Haslbeck@cern.ch
# woitout SerialCom but lighter serial
#
# 1. press the [VOLTAGE} button for 3s
# -> the only light should be (LOCK)
# -> if not, press [CURRENT] button
#
#
# FIXME: getStatus
# FIXME: enableOutput
#
# March 2020
#############################################

#from SerialCom import SerialCom
import serial
from time import sleep

    
class Tenma:
    sc = None
    def __init__(self,portname,baudrate=9600):
        self.sc = serial.Serial(portname,baudrate = baudrate)
        self.verbose=False
        pass
    
    '''
    def setVerbose(self, enable):
        self.sc.setVerbose(enable)
        self.verbose=enable
        pass
    '''

    def write(self,cmd):
        self.sc.write(cmd.encode('ascii'))
        sleep(0.1) 
        pass

    def read(self):
        out = ''
        while self.sc.inWaiting() > 0:
            out+=self.sc.read(1).decode('ascii')
            pass
        return out
    
    def readBytes(self):
        out=[]
        while self.sc.inWaiting() > 0:
            out.append(ord(self.sc.read(1)))
            pass
        return out

    def writeAndRead(self,cmd):
        self.write(cmd)
        return self.read()

    ## checkt this 
    def getStatus(self):
        command="STATUS?"
        self.write(command)
        statusBytes=self.readBytes()
        if(len(statusBytes) )<= 1:
           status  = statusBytes[0]
           ch1mode = (status & 0x01)
           output  = (status & 0x06)
           
           if ch1mode: print("ch1 in C.V mode")
           else: print("ch1 in C.C. mode")
           if output: print("output enabled")
           else: print("output disabled")
           return {'C.V 0  C.V. 1':ch1mode,'output':output}
        else:
           print("failed reading status")
           return 0
        

    def setVoltageLimit(self,iOutput,fValue): #FIXME
        self.sc.write("OVP%i %f"%(iOutput,fValue))
        pass
    
    def setCurrentLimit(self,iOutput,fValue):  #FIXME
        self.sc.write("OCP%i %f"%(iOutput,fValue))
        pass

    def setVoltage(self,V):
        ch=1
        command="VSET%i:%.2f"%(ch,V)
        return self.writeAndRead(command) 

    def setCurrent(self,A):
        ch=1
        command="ISET%i:%.3f"%(ch,A)
        print(">> setCurrent, command %s"%(command))
        self.write(command)
        sleep(0.05) #takes 50ms to respond
        self.read()
        pass

    def getVoltage(self):
        ch=1
        command = "VOUT%i?"%ch
        return self.writeAndRead(command)
        
    def getSetVoltage(self):
        ch=1
        command = "VSET%i?"%(ch)
        return self.writeAndRead(command)   
    
    def getCurrent(self): 
        ch=1
        command = "IOUT%i?"%ch
        return self.writeAndRead(command)

    def getSetCurrent(self):
        ch=1
        command = "ISET%i?"%ch
        return self.writeAndRead(command)

    ## seems more that is changing between CC and CV mode but has nothing to do with dis/enable output
    def enableOutput(self,enable):
        ch=1
        command="OUT%i"%(enable)
        self.write(command)
    
    def getVersion(self):
        return self.writeAndRead("*IDN?")
 
    def close(self):
        self.sc.close()

    def rampVoltage(self,V,steps,delay):
        vset=float(V)
        safe = True
        vold = float(self.getVoltage())
        vold = round(vold,4)

        '''
        if abs(vset-vold)<1: 
            self.setVoltage(vset)
            return
        '''
        s = 1
        steps = int(steps+1)
        vstp = float((vset-vold)/(steps-1))
        while s < (steps):
            try:
                v = self.getVoltage()
            except:
                print("Error in voltage being set")
                safe = False 
                break
            self.setVoltage(vstp*s+vold)
            sleep(delay)
            print("Ramping Voltage: %.4f"%(vstp*s+vold))
            s += 1
            pass
        pass

    pass
