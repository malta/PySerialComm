#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 11 11:10:14 2021

@author: vlad
"""


from time import sleep
from SerialCom import SerialCom

class Julabo:
    sc = None
    def __init__(self,portname,baudrate=4800,timeout=0.5):
        self.sc = SerialCom(portname,baudrate=baudrate,timeout=timeout)
        pass
    def setVerbose(self,enable):
        self.sc.setVerbose(True)
        pass
    def setSetPoint(self,temperature): 
        self.sc.write("OUT_SP_00 %.1f" % temperature)
        pass
    def getSetPoint(self):
        return self.sc.writeAndRead("IN_SP_00" + "\r\n")
    def getTemperature(self):
        return self.sc.writeAndRead("IN_PV_00")
    def toFloat(self,value):
        try: return float(value)
        except ValueError: print ("Not a float: %s"%value)
        return 0
    def getStatus(self):
        #return self.sc.writeAndRead(':'.join(hex(ord(x))[2:] for x in 'STATUS \n'))
        return self.sc.writeAndRead("STATUS \r\n")
    def setTempLimit(self,SubTemp,OverTemp):
        self.sc.write("OUT_SP_04 %.1f" % SubTemp)
        self.sc.write("OUT_SP_03 %.1f" % OverTemp)
        pass
    def getTempLimit(self):
        return [self.toFloat(self.sc.writeAndRead("IN_SP_04")),self.toFloat(self.sc.writeAndRead("IN_SP_03"))]
    
