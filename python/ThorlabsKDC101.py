##!/usr/bin/env python
#############################################
# Thorlabs KDC 101 Rotatinal Stage
# florian.Haslbeck@cern.ch
# May 2022 
# 
# Cobbled from https://github.com/AlexShkarin/pyLabLib/blob/main/pylablib/devices/Thorlabs/kinesis.py
#############################################

import serial
import struct
from time import sleep


class ThorlabsKDC101:
    """ 
    Thorlabs KDC 101 Rotational Stage 
    """
    def __init__(self,
    		    port,
                stopbits=2,
                baudrate=115200,
                timeout=3.,
                ):
        self.com = serial.Serial(port=port,baudrate=baudrate,stopbits=stopbits,timeout=timeout)
        self.com.reset_input_buffer()
    

    @staticmethod
    def cnv_degree_to_steps(input, stepsToDegree=False): 
        """ convert *input* (degrees) to steps 
            and, if *stepsToDegree*, convert steps to degrees (0.01 deg precision) )"""
    
        conv_fct = 1919.6418578623391

        # steps -> degrees
        if stepsToDegree: 
            degree = input / conv_fct
            return round(degree, 3)

        # degrees -> steps
        steps = input * conv_fct
        return int(steps)


    def send_comm(self, messageID, param1=0x00, param2=0x00, source=0x01): 
        """
        Send a message with no associated data.
        For details, see APT communications protocol.
        """
        msg=struct.pack("<HBBBB",messageID,param1,param2,0x50,source)
        self.com.write(msg)


    def send_comm_data(self, messageID, data, source=0x01): 
        """
        Send a message with associated data.
        For details, see APT communications protocol.
        """
        msg=struct.pack("<HHBB",messageID,len(data),0x50|0x80,source)
        self.com.write(msg+data)
    

    def read_comm(self): 
        """ simplified version of ``recv_comm, not implementing expected id etc. """
        msg=self.com.read(6)
        messageID,_,dest,source=struct.unpack("<HHBB",msg[:6])

        rcvd_comm = {"messageID" : messageID, "source" : source ,"dest" : dest}
        if dest&0x80:
            dest=dest&0x7F
            datalen,=struct.unpack("<H",msg[2:4])
            data=self.com.read(datalen)
            rcvd_comm['dest'] = dest 
            rcvd_comm['data'] = data
        else:
            param1,param2=struct.unpack("<BB",msg[2:4])
            rcvd_comm['param1'] = param1
            rcvd_comm['param2'] = param2
        #self.com.reset_input_buffer()
        #msg=self.com.read()
        return rcvd_comm


    def write_and_read(self, messageID, param1=0, param2=0, source=0x01):
        """ write a command to the device and read the reply 
            simplfied version of 'query' 
        """
        self.send_comm(messageID,param1=param1,param2=param2,source=source)
        return self.read_comm()


    def get_numerical_status(self):
        """
        Get numerical status of the device.
        For details, see APT communications protocol.
        """
        data = self.write_and_read(0x0429)['data'] 
        return struct.unpack("<I",data[2:6])[0]


    def get_status(self):
        """
        Get device status.
        Returns list of status strings, which can include 
        'sw_fw_lim' (forward limit switch reached), 'sw_bk_lim' (backward limit switch reached),
        'moving_fw' (moving forward), 'moving_bk' (moving backward), 'jogging_fw' (jogging forward), 'jogging_bk' (jogging backward),
        'homing' (homing), 'homed' (homing done), 'tracking', 'settled',
        'motion_error' (excessive position error), 'current_limit' (motor current limit exceeded), or 'enabled' (motor is enabled).
        """

        status_bits = [ (1<<0,"sw_bk_lim"), (1<<1,"sw_fw_lim"),
                        (1<<4,"moving_bk"),(1<<5,"moving_fw"),(1<<6,"jogging_bk"),(1<<7,"jogging_fw"),
                        (1<<9,"homing"),(1<<10,"homed"),(1<<12,"tracking"),(1<<13,"settled"),
                        (1<<14,"motion_error"),(1<<24,"current_limit"),(1<<31,"enabled")]

        numerical_status = self.get_numerical_status()
        return [s for (m,s) in status_bits if numerical_status & m]

    """
    User functions
    """

    def moveRel(self, degree=1, inSteps=False): 
        """
        Move relative *degree*(s) (positive or negative) from the current position.
        Alternatively, if *inSteps* by *degree* (as step count)
        distance : in DEGREE , default 1
        isSteps  : distance in STEPS instead of DEGREE
        """ 
        steps = int(degree) if inSteps else self.cnv_degree_to_steps(degree)
        data = struct.pack("<Hi",0,steps)
        self.send_comm_data(0x0448,data)


    def moveAbs(self, degree, inSteps=False):
        """
        Move to new postion (*degree*) from the current position.
        position : in DEGREE 
        inSteps  : new *position* is defined in steps instead
        """
        pos_step = int(degree) if inSteps else self.cnv_degree_to_steps(degree)
        data = struct.pack("<Hi",0,pos_step)
        self.send_comm_data(0x0453,data)


    def isMoving(self):
        moving_status = ["moving_fw", "moving_bk", "jogging_fw", "jogging_bk"]
        curr_status = self.get_status()
        return any([s in curr_status for s in moving_status])


    def stop(self,channel=None): # FIXME
        """ immediate stop """
        self.send_comm(0x0465)


    def waitForStop(self): 
        while self.isMoving(): sleep(.1)
        return True


    def getPos(self, inSteps=False):
        """
        Get the current postion in degrees.
        iinSteps  : position is returned in STEPS instead of DEGREES
        """
        data = self.write_and_read(0x0411)['data']
        steps = struct.unpack("<i",data[2:6])[0]
        return steps if inSteps else self.cnv_degree_to_steps(steps, stepsToDegree=True)
