#!/usr/bin/env python
import os
import sys
import Keithley
import TTi
import TTiSingle
import agilent_e3631a
import HAMEG4040

class PSUController:
    
    def __init__(self, psus):
        self.psus=psus
        for psu in psus:
            p=psus[psu]
            if "m" in p["Type"]:
                self.psus[psu]["Com"]=TTi.TTi(p["Port"])
                pass
            elif "s" in p["Type"]:
                self.psus[psu]["Com"]=TTiSingle.TTiSingle(p["Port"])
                pass
            elif "k" in p["Type"]:
                self.psus[psu]["Com"]=Keithley.Keithley(p["Port"])
                pass
            elif "a" in p["Type"]:
                self.psus[psu]["Com"]=agilent_e3631a.agilent_e3631a(p["Port"])
                pass    
            elif "h" in p["Type"]:
                self.psus[psu]["Com"]=HAMEG4040.HAMEG4040(p["Port"])
                pass    
            print (psu, p["Port"], self.psus[psu]["Com"].getModel())
            pass
        
        pass
    
    def getSetVoltage(self, psu):
        p=self.psus[psu]
        if p["Type"] in ("s","k","a"): return p["Com"].getSetVoltage()
        elif p["Type"] in ("m","h"): return p["Com"].getSetVoltage(p["Chan"])
        return 0

    def getVoltageLimit(self, psu):
        p=self.psus[psu]
        if p["Type"] in ("s","k","a"): return p["Com"].getVoltageLimit()
        elif p["Type"] in ("m","h"): return p["Com"].getVoltageLimit(p["Chan"])
        return 0

    def getCurrentLimit(self, psu):
        p=self.psus[psu]
        if p["Type"] in ("s","k","a"): return p["Com"].getSetCurrent()
        elif p["Type"] in ("m","h"): return p["Com"].getSetCurrent(p["Chan"])
        return 0

    def getVoltage(self, psu):
        p=self.psus[psu]
        if p["Type"] in ("s","k"): return p["Com"].getVoltage()
        elif p["Type"] in ("m","a","h"): return p["Com"].getVoltage(p["Chan"])
        return 0

    def getCurrent(self, psu):
        p=self.psus[psu]
        if p["Type"] in ("s","k"): return p["Com"].getCurrent()
        elif p["Type"] in ("m","a","h"): return p["Com"].getCurrent(p["Chan"])
        return 0

    def isEnabled(self, psu):
        p=self.psus[psu]
        if p["Type"] in ("s","k","a"): return p["Com"].isEnabled()
        elif p["Type"] in ("m","h"): return p["Com"].isEnabled(p["Chan"])
        return False

    def setOutput(self, psu, val):
        p=self.psus[psu]
        if p["Type"] in ("s","k","a"): return p["Com"].enableOutput(val)
        elif p["Type"] in ("m","h"): return p["Com"].enableOutput(p["Chan"],val)
        pass

    def setVoltage(self, psu, val):
        p=self.psus[psu]
        if p["Type"] in ("s","k","a"): return p["Com"].setVoltage(val)
        elif p["Type"] in ("m","h"): return p["Com"].setVoltage(p["Chan"],val)
        pass

    def rampVoltage(self, psu, val):
        p=self.psus[psu]
        val0=self.getVoltage(psu)
        if p["Type"] in ("s","k","a"): return p["Com"].rampVoltage(val,3,0.1)
        elif p["Type"] in ("m","h"): return p["Com"].rampVoltage(p["Chan"],val,3,0.1)
        pass

    def setVoltageLimit(self, psu, val):
        p=self.psus[psu]
        if p["Type"] in ("s","k","a"): return p["Com"].setVoltageLimit(val)
        elif p["Type"] in ("m","h"): return p["Com"].setVoltageLimit(p["Chan"],val)
        pass

    def setCurrentLimit(self, psu, val):
        p=self.psus[psu]
        if p["Type"] in ("s","k","a"): return p["Com"].setCurrentLimit(val)
        elif p["Type"] in ("m","h"): return p["Com"].setCurrentLimit(p["Chan"],val)
        pass
    
    pass
