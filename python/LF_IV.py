#!/usr/bin/env python

import SerialCom
import Keithley
import time
import ROOT

keith = Keithley.Keithley("/dev/cu.usbserial-FTXG6Y8TA")#FTZ5U8CXB
keith.setCurrentLimit(20.0)
keith.setVoltageLimit(200)
keith.enableOutput(True)
keith.setVoltage(-1)
volts = keith.getVoltage()
curr = keith.getCurrent()

timestr = time.strftime("%Y_%m_%d-%H%M%S")
print timestr

f = open(timestr+"txt",'w')
data = {}

while volts <= 200:
    curr = keith.getCurrent()
    volts = keith.getVoltage()
    svolts = str(volts)
    scurr = str(curr)
    print "volts: "+svolts,"current: "+scurr
    f.write(svolts+"\t"+scurr+"\n")
    volts += 1
    keith.setVoltage(volts)
    time.sleep(1)

f.close()
keith.enableOutput(False)

ROOT.gStyle.SetPadLeftMargin(0.134)
ROOT.gStyle.SetPadBottomMargin(0.146)
ROOT.gStyle.SetPadRightMargin(0.15)
ROOT.gStyle.SetPadTopMargin(0.078)
ROOT.gStyle.SetLabelSize(0.05,"x")
ROOT.gStyle.SetLabelSize(0.05,"y")
ROOT.gStyle.SetTitleSize(0.05,"t")
ROOT.gStyle.SetTitleSize(0.05,"x")
ROOT.gStyle.SetTitleSize(0.05,"y")
ROOT.gStyle.SetTitleOffset(1.40,"y")
ROOT.gStyle.SetOptStat(0);

f = open(timestr+"txt", "r") 

g1=ROOT.TGraph()

for line in f.readlines():
    line = line.strip()
    volts = float(line.split()[0])
    current = float(line.split()[1])
    print "volts: ",volts, "current: ",current
    
    g1.SetPoint(g1.GetN(),volts,current)
    pass
f.close()

g1.SetLineColor(ROOT.kRed)
g1.SetMarkerColor(ROOT.kRed)
g1.SetMarkerStyle(20)
g1.SetMarkerSize(1.05)

g1.Draw("AP")
#g1.GetXaxis().SetRangeUser(-20,50)

raw_input()
