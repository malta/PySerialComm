# -*- coding: utf-8 -*-

'''
@author: Dingane Haluku, dingane,reward.hlaluku@cern.ch
Date: 29 May 2016
******************************************************
Modules for the Iseg HV power supply diagnostics.
'''

from SerialCom import SerialCom

class Iseg:
    sc = None
    cmd = ""
    
    # two Channel numbers, 1 and 2    
    def __init__(self, portname='/dev/ttyUSB0'):
        self.portname = portname
        self.sc = SerialCom(portname, baudrate = 9600)
        pass
        
    def close(self, sc):
        self.sc = sc
        self.sc.close()
    # Read module identifier
    def getModuleID(self):
        cmd = "#"
        moduleID = self.sc.writeAndRead(cmd)   # Write and read to Iseg in order to receive module ID
        readModID = self.sc.read()
        return readModID;     
        
    # Read break time
    def getBreakTime(self):
        cmd = 'W'
        breakTime = self.sc.writeAndRead(cmd)
        readBreakTime = self.sc.read()
        return int(readBreakTime);
        
    # Write break time
    def setBreakTime(self,myBreakTime):
        if myBreakTime<2 or myBreakTime>255: return False
        cmd = 'W%i'%myBreakTime
        setTime = self.sc.writeAndRead(cmd)
        readTime = self.sc.read()
        return True;
        
    # Read actual voltage
    def getVoltage(self,channel):
        cmd = "U%i"%channel
        v = self.sc.writeAndRead(cmd)
        v1 = self.sc.read()
        vf=float(v1[:-3])*10**float(v1[-3:])
        return vf;
        
    # Read actual current for each channel number
        
    def setCurrentLimit(self,channel,fvalAmps):        
        if fvalAmps > 1 : 
            cmd="L%i=%5i" % (channel,int(fvalAmps)) #amps
        elif fvalAmps < 0.001:
            cmd="LS%i=%5i"%(channel,int(fvalAmps*1E6)) #micro-amps
        else:
            cmd="LB%i=%5i"%(channel,int(fvalAmps*1E3)) #milli-amps
        #self.sc.write(cmd)        
        ret=self.sc.writeAndRead(cmd)
        print "Iseg::setCurrentLimit return %s" % ret
        return cmd;
        
    def setVoltage(self,channel,fvalVolts):
        cmd="D%i=%4.2f"%(channel,fvalVolts)
        ret=self.sc.writeAndRead(cmd)
        voltStatus=self.sc.read()
        print 'The current set voltage status is: ',voltStatus
        print "Iseg::setVoltage write: %s, read: %s\n" % (cmd,ret)
        pass
        
    def enableOutput(self,channel):
        cmd="G%i"%channel
        ret=self.sc.writeAndRead(cmd)
        status=self.sc.read()
        print "Iseg::enableOutput write: %s, read: %s, status %s\n" % (cmd,ret,status[3:])        
        return status[3:]
        
    
        
    def getCurrent(self,channel):
        v = self.sc.writeAndRead("I%i"%channel)
        print "To be fixed: {mantisse/exp with sign}: ", v
        return v;
        
    def getCurrentLimit(self, channel):
        v = self.sc.writeAndRead("L%i"%channel)
        print "To be fixed: {mantisse/exp with sign}: ", v
        return v;
    pass
