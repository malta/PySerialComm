#!/usr/bin/env python
from SerialCom import SerialCom
import time

# putting sleeps in between commands is a bit error prone, especially for writeAndRead operations
# however, checking the device readiness with *OCP? at every command is super slow...

class HAMEG4040(object):
  sc = None
  def __init__(self,portname,baudrate=9600):
    self.sc = SerialCom(portname,baudrate = baudrate)

  def setVerbose(self,enable):
    self.sc.setVerbose(enable)
    pass
  
  def getModel(self):
    return self.sc.writeAndRead("*IDN?")

  def getVersion(self):
    return self.sc.writeAndRead("syst:vers?")

  def setChannel(self, channel):
    self.sc.write("inst out{:d}".format(channel))
    time.sleep(0.1)
    #self.sc.writeAndRead("*OPC?")
    #channelReadback = int(self.sc.writeAndRead("inst:sel?").lower().strip("outp"))
    #if int(channelReadback) != int(channel):
    #  raise RuntimeError("channel error:","set {} but read back {}".format(channel, channelReadback))

  def isEnabled(self, channel):
    self.setChannel(channel)
    self.sc.writeAndRead("*OPC?")
    if self.sc.writeAndRead("outp?") == '1':
      return True
    else:
      return False

  def toFloat1(self,string):
    ret = None
    if len(string) > 2: ret = float(string[2:])
    return ret

  def toFloat2(self,string):
    ret = None
    if len(string)> 1: ret = float(string[:-1])
    return ret

  def setVoltageLimit(self, channel, val):
    self.setChannel(channel)
    self.sc.write("volt:prot {:f}".format(val))
    time.sleep(0.1)

  def setCurrentLimit(self, channel, val):
    self.setChannel(channel)
    self.sc.write("curr:prot {:f}".format(val))
    time.sleep(0.1)

  def setCurrent(self, channel, val):
    self.setChannel(channel)
    self.sc.write("curr {:f}".format(val))
    time.sleep(0.1)

  def setVoltage(self, channel, val):
    self.setChannel(channel)
    self.sc.write("volt {:f}".format(val))
    time.sleep(0.1)

  def getVoltageLimit(self, channel):
    self.setChannel(channel)
    self.sc.writeAndRead("*OCP?")
    return self.sc.write("volt:prot?")

  def getSetVoltage(self, channel):
    self.setChannel(channel)
    self.sc.writeAndRead("*OCP?")
    return float(self.sc.writeAndRead("volt?"))

  def getSetCurrent(self, channel):
    self.setChannel(channel)
    self.sc.writeAndRead("*OCP?")
    return float(self.sc.writeAndRead("curr?"))

  def getVoltage(self, channel):
    self.setChannel(channel)
    self.sc.writeAndRead("*OCP?")
    return float(self.sc.writeAndRead("meas:volt?"))

  def getCurrent(self, channel):
    self.setChannel(channel)
    self.sc.writeAndRead("*OCP?")
    return float(self.sc.writeAndRead("meas:curr?"))
  
  def getCurrentLimit(self, channel):
    return self.getSetCurrent()
  
  def enableOutput(self, channel, val):
    self.setChannel(channel)
    if val:
      self.sc.write("outp on")
    else:
      self.sc.write("outp off")
    time.sleep(0.1)
    #self.sc.writeAndRead("*OCP?")

  def close(self):
    self.sc.close()
  
  def rampVoltage(self, channel, val, iSteps, iSetDelay):
    vset=float(val)
    safe = True
    vold = self.getVoltage(channel)
    vold = round(vold,4)
    if abs(vset-vold)<1: 
      self.setVoltage(channel,vset)
      return
    s = 1
    iSteps = int(iSteps+1)
    vstp = float((vset-vold)/(iSteps-1))
    while s < (iSteps):
      v = self.getVoltage(channel)
      if v == -999:
        print("Error in voltage being set")
        safe = False 
        pass
      if safe == False: break
      self.setVoltage(channel,vstp*s+vold)
      time.sleep(iSetDelay)
      print("Ramping Voltage: %.4f"%(vstp*s+vold))
      s += 1
      pass
    pass

