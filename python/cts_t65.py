#!/usr/bin/env python
#############################################
# CTS T-65 climate chamber control
# Florian.Dachs@cern.ch
# January 2021
#############################################

import codecs
import serial
import logging
from datetime import datetime

class CTS_T65:
    sc = None
    
    logging.basicConfig(level=logging.INFO)
    
    def __init__(self,portname):
        self.sc = serial.Serial(portname,
                                baudrate = 19200,
                                bytesize=8,
                                parity=serial.PARITY_ODD,
                                timeout=1.0,
                                xonxoff=False,
                                rtscts=False,
                                dsrdtr=False,
                                write_timeout=1.0,
                                inter_byte_timeout=None)
        pass

    def buildCheckSum(self,data):
        check = data[0]
        for i in range(1,len(data)):
            check = check^data[i]
        check = check|0x80
        return check
        
    def getReply(self):
        response = bytearray(self.sc.read_until(chr(0x03)))
        for i in range(len(response)):
            response[i] = (response[i]&0x7F)
        return response
		
    def decodeTime(self,response):
        msg = response[3:15].decode('utf-8')

        date = datetime(int(msg[4:6])+2000,\
                        int(msg[2:4]),\
                        int(msg[0:2]),\
                        int(msg[6:8]),\
                        int(msg[8:10]),\
                        int(msg[10:12]))
                        
        date = datetime.timestamp(date)
        return date
		
    
    def getTime(self,adr=1):
        cmd = bytearray(5)
        cmd[0] = 0x02
        cmd[1] = (int(adr)|0x80)
        cmd[2] = (ord('T')|0x80)
        cmd[3] = self.buildCheckSum(cmd[1:3])
        cmd[4] = 0x03

        self.sc.write(cmd)
        reply = self.getReply()

        return self.decodeTime(reply)

    def setTime(self,date=datetime.now(),adr=1):
        cmd = bytearray(17)
        cmd[0] =  0x02
        cmd[1] =  (int(adr)|0x80)
        cmd[2] =  (ord('t')|0x80)
        if date.day < 10:
            cmd[3] =  (ord(str(0)[0])|0x80)
            cmd[4] =  (ord(str(date.day)[0])|0x80)
        else:
            cmd[3] =  (ord(str(date.day)[0])|0x80)
            cmd[4] =  (ord(str(date.day)[1])|0x80)
        if date.month < 10:
            cmd[5] =  (ord(str(0)[0])|0x80)
            cmd[6] =  (ord(str(date.month)[0])|0x80)
        else:
            cmd[5] =  (ord(str(date.month)[0])|0x80)
            cmd[6] =  (ord(str(date.month)[1])|0x80)
        cmd[7] =  (ord(str(date.year)[2])|0x80)
        cmd[8] =  (ord(str(date.year)[3])|0x80)
        if date.hour < 10:
            cmd[9] =  (ord(str(0)[0])|0x80)
            cmd[10] =  (ord(str(date.hour)[0])|0x80)
        else:
            cmd[9] =  (ord(str(date.hour)[0])|0x80)
            cmd[10] =  (ord(str(date.hour)[1])|0x80)
        if date.minute < 10:
            cmd[11] =  (ord(str(0)[0])|0x80)
            cmd[12] =  (ord(str(date.minute)[0])|0x80)
        else:
            cmd[11] =  (ord(str(date.minute)[0])|0x80)
            cmd[12] =  (ord(str(date.minute)[1])|0x80)
        if date.second < 10:
            cmd[13] =  (ord(str(0)[0])|0x80)
            cmd[14] =  (ord(str(date.second)[0])|0x80)
        else:
            cmd[13] =  (ord(str(date.second)[0])|0x80)
            cmd[14] =  (ord(str(date.second)[1])|0x80)
        cmd[15] = self.buildCheckSum(cmd[1:15])
        cmd[16] = 0x03

        self.sc.write(cmd)
        reply = self.getReply()

        return self.decodeTime(reply)

    def getSetTemperature(self,adr=1):
        cmd = bytearray()
        cmd.append(0x02)
        cmd.append(int(adr)|0x80)
        cmd.append(ord('A')|0x80)
        cmd.append(ord('0')|0x80)
        cmd.append(self.buildCheckSum(cmd[1:]))
        cmd.append(0x03)

        self.sc.write(cmd)
        reply = self.getReply()

        msg = reply[3:-2].decode('utf-8')

        return float(msg[8:13])

    def getActualTemperature(self,adr=1):
        return self.getTemperature(adr)

    def getTemperature(self,adr=1):
        cmd = bytearray()
        cmd.append(0x02)
        cmd.append(int(adr)|0x80)
        cmd.append(ord('A')|0x80)
        cmd.append(ord('0')|0x80)
        cmd.append(self.buildCheckSum(cmd[1:]))
        cmd.append(0x03)

        self.sc.write(cmd)
        reply = self.getReply()

        msg = reply[3:-2].decode('utf-8')

        return float(msg[2:7])
        

    def setTemperature(self,temp,adr=1,readBack=True):        
        #check temperature input
        if not isinstance(temp,float):
            try:
                temp = float(temp)
            except Exception as e:
                logging.error(e)
                logging.error("Provide temperature as float value!")
                exit()

        if temp < -99.0 or temp > 200.0:
            logging.error("Provide a temperature between -99.0 and 200.0 C!")
            return None

        # temp = "{:.1f}".format(temp)
        str_temp = str(temp).zfill(5)

        cmd = bytearray()
        cmd.append(0x02)
        cmd.append(int(adr)|0x80)
        cmd.append(ord('a')|0x80)
        cmd.append(ord('0')|0x80)
        cmd.append(ord(' ')|0x80)
        for i in range(len(str_temp)):
        	cmd.append(ord(str_temp[i])|0x80)
        cmd.append(self.buildCheckSum(cmd[1:]))
        cmd.append(0x03)

        self.sc.write(cmd)
        reply = self.getReply()

        msg = reply[2:3].decode('utf-8')
        
        if msg == 'a':
        	logging.info("Set temperature to "+str(temp))
        else:
        	logging.warning("Unexpected reply from machine: ",msg)

        if readBack:
        	return self.getSetTemperature(adr)

    def readStatus(self,n=1,adr=1):
        if n > 9 or n < 1:
            logging.warning("Status bits only available from 0 to 8")
            return -1;

        cmd = bytearray()
        cmd.append(0x02)
        cmd.append(int(adr)|0x80)
        cmd.append(ord('S')|0x80)
        cmd.append(self.buildCheckSum(cmd[1:]))
        cmd.append(0x03)

        self.sc.write(cmd)
        reply = self.getReply()

        n = n-1 #shift by 1 to start counting at 0
        msg = reply[3+n:4+n].decode('utf-8')
        
        return int(msg)


    def readError(self,adr=1):
        cmd = bytearray()
        cmd.append(0x02)
        cmd.append(int(adr)|0x80)
        cmd.append(ord('F')|0x80)
        cmd.append(self.buildCheckSum(cmd[1:]))
        cmd.append(0x03)

        self.sc.write(cmd)
        reply = self.getReply()

        msg = reply[3:-2].decode('utf-8')
        
        return msg

    def quitError(self,adr=1,readBack=True):
        cmd = bytearray()
        cmd.append(0x02)
        cmd.append(int(adr)|0x80)
        cmd.append(ord('s')|0x80)
        cmd.append(ord('2')|0x80)
        cmd.append(ord(' ')|0x80)
        cmd.append(ord('0')|0x80)
        cmd.append(self.buildCheckSum(cmd[1:]))
        cmd.append(0x03)

        self.sc.write(cmd)
        reply = self.getReply()

        msg = reply[2:3].decode('utf-8')
                
        if msg == 's':
            logging.warning("QUIT ERROR acknowledged!")
        else:
            logging.warning("Unexpected reply from machine: ",msg)

        if readBack:
            return self.readStatus(adr,9)

    def start(self,adr=1,readBack=True):
        cmd = bytearray()
        cmd.append(0x02)
        cmd.append(int(adr)|0x80)
        cmd.append(ord('s')|0x80)
        cmd.append(ord('1')|0x80)
        cmd.append(ord(' ')|0x80)
        cmd.append(ord('1')|0x80)
        cmd.append(self.buildCheckSum(cmd[1:]))
        cmd.append(0x03)

        self.sc.write(cmd)
        reply = self.getReply()

        msg = reply[2:3].decode('utf-8')
                
        if msg == 's':
            logging.info("START acknowledged!")
        else:
            logging.error("Unexpected reply from machine: ",msg)

        if readBack:
            return self.readStatus(adr,1)

    def stop(self,adr=1,readBack=True):
        cmd = bytearray()
        cmd.append(0x02)
        cmd.append(int(adr)|0x80)
        cmd.append(ord('s')|0x80)
        cmd.append(ord('1')|0x80)
        cmd.append(ord(' ')|0x80)
        cmd.append(ord('0')|0x80)
        cmd.append(self.buildCheckSum(cmd[1:]))
        cmd.append(0x03)

        self.sc.write(cmd)
        reply = self.getReply()

        msg = reply[2:3].decode('utf-8')
                
        if msg == 's':
            logging.info("STOP acknowledged!")
        else:
            logging.error("Unexpected reply from machine: ",msg)

        if readBack:
            return self.readStatus(adr,1)
        
    def isOn(self,adr=1):
        return (self.readStatus(adr,1) == 1)

    def isOff(self,adr=1):
        return (self.readStatus(adr,1) == 0)

'''
if __name__ == "__main__":
    comms = CTS_T65("/dev/serial/by-id/usb-FTDI_FT232R_USB_UART_A907R8NF-if00-port0")
    print(comms.setTemperature(25.0))
'''
