#!/usr/bin/env python
#############################################
# TTI power supply control
#
# Carlos.Solans@cern.ch
# Abhishek.Sharma@cern.ch
# February 2016
#############################################

from SerialCom_Glasgow import SerialCom
    
class TTi_Glasgow:
    sc = None
    def __init__(self,portname):
        self.sc = SerialCom(portname,baudrate = 9600)

    def setVoltageLimit(self,iOutput,fValue):
        self.sc.write("OVP%i %f"%(iOutput,fValue))

    def setCurrentLimit(self,iOutput,fValue):
        self.sc.write("OCP%i %f"%(iOutput,fValue))

    def setVoltage(self,iOutput,fValue):
        self.sc.write("V%i %f"%(iOutput,fValue))

    def getVoltage(self,iOutput):
        return self.sc.writeAndRead("V%i?"%iOutput)

    def getCurrent(self,iOutput):
        return self.sc.writeAndRead("I%i?"%iOutput)

    def enableOutput(self,iOutput,bValue):
        if bValue == True:
            self.sc.write("OP%i 1"%iOutput)
        elif bValue == False:
            self.sc.write("OP%i 0"%iOutput)

    def close(self):
        self.sc.close()
