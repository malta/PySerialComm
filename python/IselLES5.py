#!/usr/bin/env python
#############################################
# ISEL Linear Stage control
# Model LES5
# Abhishek.Sharma@cern.ch
# October 2019
# April 2022 - Migrate to python 3
#############################################
from SerialCom import SerialCom

class IselLES5:

    sc = None
    def __init__(self,portname,baudrate=19200):
        self.sc = SerialCom(portname,baudrate=baudrate)
        self.min=0
        self.max=496
        self.steps=0
        self.invert=False
        self.speed=5
        self.acceleration=10
        self.verbose=False
        pass

    def configureController(self, numberOfAxis=1):
        if self.verbose: print ("Configuring Controller.")
        cmd = "@0" + str(numberOfAxis) + "\r"
        self.sc.write(cmd)
        self.setLimits(self.min,self.max)
        pass

    def setVerbose(self, enable):
        self.verbose=verbose
        self.sc.setVerbose(enable)
        pass

    def mmToSteps(self,val):
        self.steps=int((val*1.)/0.006239)
        return self.steps

    def stepsToMM(self,val):
        self.steps=float("%.2f"%(val*0.006239))
        return self.steps

    def setSpeed(self, speed=5):
        self.speed=speed
        print ("setting reference speed to %i mm/sec."%speed)
        speed=self.mmToSteps(speed)        
        cmd = "@0d" + str(speed) + "\r"
        self.sc.write(cmd)
        print (self.sc.writeAndRead("@0b\r"))
        pass

    def setAcceleration(self, acceleration=5):
        self.acceleration=self.mmToSteps(acceleration)
        print ("setting reference acceleration to %i mm/sec^2"%acceleration)
        cmd = "@0g" + str(acceleration) + "\r"
        self.sc.write(cmd)
        print (self.sc.writeAndRead("@0b\r"))
        pass

    def setCurrentPointAsZero(self,position):
        if self.invert:
            position=523-position
            pass
        position=self.mmToSteps(position)
        print ("setting %i mm as new zero point."%self.getCurrentPosition())
        cmd = "@0n1\r"
        self.sc.write(cmd)
        print (self.sc.writeAndRead("@0b\r"))
        pass
        
    def getCurrentPosition(self):
        pos=self.stepsToMM(int((self.sc.writeAndRead("@0P\r")),16))
        if self.invert: pos=self.max-pos
        if self.verbose: print ("Current position is: %s mm."%pos)
        return pos

    def moveToPositionAbsolute(self,position):
        if position<self.min or position>self.max:
            print("Position outside range [%s,%s]: %s" %(self.min,self.max,position))
            return 
        print ("Moving to absolute position [mm]: %i"%position)
        if self.invert: position=self.max-position
        pos=self.mmToSteps(position)
        spe=self.mmToSteps(self.speed)
        cmd = "@0M" + str(pos) + "," + str(spe) + "\r"
        self.sc.writeAndRead(cmd)
        while self.sc.writeAndRead("@0b\r") != '7':
            print ("moving...")
            pass
        pass

    def moveToPositionRelative(self, position):
        curPos=self.getCurrentPosition()
        if (curPos+position)<self.min or (curPos+position)>self.max:
            print("Position outside range [%s,%s]: %s" %(self.min,self.max,position+curPos))
            return
        print ("Moving to relative position [mm]: %i"%position)
        if self.invert: position=-1*position
        pos=self.mmToSteps(position)
        spe=self.mmToSteps(self.speed)
        cmd = "@0A" + str(pos) + "," + str(spe) + "\r"
        self.sc.writeAndRead(cmd)
        while self.sc.writeAndRead("@0b\r") != '7':
            print ("moving...")
            pass
        pass

    def getVersion(self):
        return self.sc.writeAndRead("@0V")

    def readPort(self):
        print ("reading port")
        return self.sc.writeAndRead("@0b\r")
        pass

    def runReferenceRun(self):
        print ("performing reference run...")
        self.sc.write("@0R1")
        while self.sc.writeAndRead("@0b\r") != '7':
            print ("running...")
            pass
        pass

    def setLimits(self,min,max):
        self.min=min
        self.max=max
        pass

    def invertAxis(self,doinvert):
        self.invert=doinvert
        return self.invert

    def close(self):
        self.sc.close()

    pass
