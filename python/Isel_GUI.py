#!/usr/bin/env python
#######################################
# Isel LES5 Linear Stage Controller GUI
#
# Abhishek.Sharma@cern.ch
# October 2019
#######################################



import sys
from PyQt5.QtWidgets import QWidget, QLabel, QApplication,QInputDialog,QLineEdit,QPushButton,QApplication, QMainWindow
from PyQt5.QtGui import QIcon, QPixmap
from PyQt5.Qt import Qt
from PyQt5.QtCore import QEvent
import IselLES5
import time

class Example(QMainWindow):
    
    def __init__(self):
        super(Example,self).__init__()
        self.title = "ISEL Linear Stage Control"
        self.left = 10
        self.top = 10
        self.width = 572
        self.height = 430
        self.initUI()
            
    def initUI(self):
        self.setGeometry(self.left, self.top, self.width, self.height)

        self.motorx = IselLES5.IselLES5("/dev/serial/by-id/usb-FTDI_FT232R_USB_UART_AH06QAV3-if00-port0")
        self.motorz = IselLES5.IselLES5("/dev/serial/by-id/usb-FTDI_FT232R_USB_UART_AH06QAV5-if00-port0")

        self.motorx.invertAxis(True)
        self.motorz.invertAxis(True)
        
        #self.motorx.moveToPositionAbsolute(48.02)
        #self.motorz.moveToPositionAbsolute(38.01)

        self.motorx.setLimits(42.02,495)
        #self.motorz.setLimits(25,480)
        
        #label = QLabel(self)
        #pixmap = QPixmap('GUI.png')
        #label.setPixmap(pixmap)
        
        label = QLabel(self)
        #label.move(10,10)
        label.resize(572,430)
        pixmap = QPixmap('GUI2.png')
        label.setPixmap(pixmap)
        #self.resize(pixmap.width(), pixmap.height())

        #lay.addWidget(label)

        # Optional, resize window to image size
        #self.resize(pixmap.width(),pixmap.height())
        '''
        self.currPosTitle = QLabel('Current\nPosition', self)
        self.currPosTitle.move(225, 10)        

        self.xPosTitle = QLabel('X position', self)
        self.xPosTitle.move(25, 45)

        self.zPosTitle = QLabel('Z position', self)
        self.zPosTitle.move(25, 75)

        self.xPos = QLineEdit(self)
        self.xPos.move(120, 45)

        self.zPos = QLineEdit(self)
        self.zPos.move(120, 75)

        self.upB = QPushButton('Up', self)
        self.upB.move(185,190)
        #while button.pressed.connect()
        self.upB.clicked.connect(self.up)

        self.downB = QPushButton('Down', self)
        self.downB.move(185,260)        
        self.downB.clicked.connect(self.down)

        self.rightB = QPushButton('Right (Towards Exit)', self)
        self.rightB.resize(160,30)
        self.rightB.move(300,225)        
        self.rightB.clicked.connect(self.rightb)

        self.leftB = QPushButton('Left (Away from Exit)', self)
        self.leftB.resize(160,30)
        self.leftB.move(10,225)        
        self.leftB.clicked.connect(self.leftb)


        #self.confB = QPushButton('Configure', self)
        #self.confB.move(15,10)        
        #self.confB.clicked.connect(self.conf)

        #self.readB = QPushButton('Read', self)
        #self.readB.move(120,10)        
        #self.readB.clicked.connect(self.read)

        #self.moveB = QPushButton('Move', self)
        #self.moveB.move(15,105)        
        #self.moveB.clicked.connect(self.move)

        #self.quitB = QPushButton('Quit', self)
        #self.quitB.move(220,105)        
        #self.quitB.clicked.connect(self.close)

        self.xPosCurr = QLabel('        ', self)
        self.xPosCurr.move(235, 45)

        self.zPosCurr = QLabel('        ', self)
        self.zPosCurr.move(235, 75)
        '''
        self.setWindowTitle(self.title)
        
        #self.read
        
        self.show()

        for i in xrange(5):
            print "The keyboard arrow buttons will be activated in %i seconds!"%(5-i)
            if i==4:
                print "The keyboard arrows are now linked to the linear stage!!!"
                pass
            time.sleep(2)
            pass
        pass

    def keyPressEvent(self, event): 
        if event.key() == Qt.Key_Up and event.isAutoRepeat()==False:
            if event.type()==QEvent.KeyPress: self.upb()
        if event.key() == Qt.Key_Down and event.isAutoRepeat()==False:
            if event.type()==QEvent.KeyPress: self.downb()
        if event.key() == Qt.Key_Left and event.isAutoRepeat()==False:
            if event.type()==QEvent.KeyPress: self.leftb()
        if event.key() == Qt.Key_Right and event.isAutoRepeat()==False:
            if event.type()==QEvent.KeyPress: self.rightb()
        if event.key() == Qt.Key_Space and event.isAutoRepeat()==False:
            if event.type()==QEvent.KeyPress: self.home()

    def tmp(self):
        print "ok"

    def upb(self):
        print "Moving UP by 1cm"
        self.motorz.moveToPositionRelative(10)
        pass

    def downb(self):
        print "Moving DOWN by 1cm"
        self.motorz.moveToPositionRelative(-10)
        pass

    def rightb(self):
        print "Moving TOWARDS EXIT by 1cm"
        self.motorx.moveToPositionRelative(10)
        pass

    def leftb(self):
        print "Moving AWAY from EXIT by 1cm"
        self.motorx.moveToPositionRelative(-10)
        pass

    def home(self):
        #if raw_input("Are you SURE you want to return to cold box to original alignment position in X AND Z? if yes, type YES")=="YES":
            #if raw_input("Confirm with yes")=="yes":
        self.motorx.setSpeed(5)
        self.motorz.setSpeed(5)
        print "GOING TO HOME POSITION"
        self.motorx.moveToPositionAbsolute(48.02)
        self.motorz.moveToPositionAbsolute(38.01)
        pass
        #pass
        #pass


    '''
    def conf(self):
        self.motorx.configureController(1)
        self.motorx.setSpeed(20)
        self.motorz.configureController(1)
        self.motorz.setSpeed(20)
        self.statusBar().showMessage("Configuring Stages.")
        pass

    def read(self):
        x=self.motorx.getCurrentPosition()
        z=self.motorz.getCurrentPosition()
        self.xPosCurr.setText("%s mm"%x)
        self.xPos.setText("%s"%x)
        self.zPosCurr.setText("%s mm"%z)
        self.zPos.setText("%s"%z)
        pass

    def move(self):
        self.motorx.moveToPositionAbsolute(float(self.xPos.text()))
        self.motorz.moveToPositionAbsolute(float(self.zPos.text()))      
        self.read()
        pass

    def invert(self):
        self.motorx.invertAxis(True)
        self.motorz.invertAxis(True)
        print "axes inverted."
        pass
       
    '''

if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = Example()
    sys.exit(app.exec_())
