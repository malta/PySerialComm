#!/usr/bin/env python
#############################################
# Agilent_E3646A power supply control
#
# Carlos.Solans@cern.ch
# Abhishek.Sharma@cern.ch
# zainulabedin.akuji@cern.ch
# July 2018
#############################################

from time import sleep
from SerialCom import SerialCom

class agilent_e3646a:
    sc = None
    def __init__(self,portname):
        self.sc = SerialCom(portname,baudrate = 9600)

    def enableRemote(self):
        self.sc.write("SYST:REM")

    def setOutput(self,iOutput):
        self.sc.write("INST:NSEL %i"%(iOutput))

    def setVoltage(self,fValue):
        self.sc.write("VOLT %f"%(fValue))

    def rampVoltage(self,startV,endV,steps,delay):
        Vstep = (endV*1.-startV*1.)/steps
        for i in range(steps+1):
            self.setVoltage(startV + i*Vstep)
            sleep(delay)

    def setCurrentLimit(self,fValue):
        self.sc.write("CURR %f"%(fValue))

    def getVoltage(self):
        self.sc.writeAndRead("VOLT?")
        return self.sc.writeAndRead("VOLT?")

    def getCurrent(self):
        self.sc.writeAndRead("MEAS:CURR?")
        return self.sc.writeAndRead("MEAS:CURR?")

    def enableOutput(self,bValue):
        if bValue == True:
            self.sc.write("OUTP 1")
        elif bValue == False:
            self.sc.write("OUTP 0")

    def systemError(self):
        return self.sc.writeAndRead("SYST:ERR?")

    def reset(self):
        return self.sc.writeAndRead("*RST")

    def close(self):
        self.sc.close()





