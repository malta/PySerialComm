#!/usr/bin/env python
#############################################
# TTI TSX3510P single power supply control
#
# Carlos.Solans@cern.ch
# Abhishek.Sharma@cern.ch
# Sascha.Dungs@cern.ch
# September 2016
#############################################

from SerialCom import SerialCom
import serial
import io

class TTiSingle(SerialCom):
    
    def __init__(self,portname):
        ## self.com = serial.Serial()
        ## self.com.port=portname
        ## self.com.baudrate=9600
        ## self.com.bytesize=8
        ## self.com.parity='N'
        ## self.com.stopbits=1 
        ## self.com.dtr=False
        ## self.com.rts=False
        ## self.com.xonxoff=True
        ## self.com.timeout=0.1
        ## self.com.open()
        self.com = serial.Serial(port=portname,
                                 baudrate=9600,
                                 timeout=0.5,
                                 bytesize=8,
                                 parity='N',
                                 stopbits=1,
                                 xonxoff=True)
        self.com.dtr=False
        self.com.rts=False
        self.sio = io.TextIOWrapper(io.BufferedRWPair(self.com,self.com),
                                    errors='ignore')
        self.trm = '\r\n'
        self.verbose = False
        pass
    
    def getModel(self):
        return self.writeAndRead("*IDN?")
    
    def isEnabled(self, iOutput):
        if self.writeAndRead("OP%i?" % iOutput) == "1":
            return True
        else:
            return False
        pass
    
    def toFloat1(self,str):
        ret=None
        if len(str)>2: ret=float(str[2:])
        return ret

    def toFloat2(self,str):
        ret=None
        if len(str)>1: ret=float(str[:-1])
        return ret
    
    def setVoltageLimit(self,fValue):
        self.write("OVP %f"%fValue)
        pass
    
    def reset(self):
        self.write("*RST")
        pass
    
    def setCurrentLimit(self,fValue):
        self.write("OCP %f"%fValue)
        pass
    
    def setVoltage(self,fValue):
        self.write("V %f"%fValue)
        pass
    
    def getSetVoltage(self):
        return self.toFloat1(self.writeAndRead("V?"))
    
    def getVoltage(self):
        return self.toFloat2(self.writeAndRead("VO?"))
    
    def getPower(self):
        return self.toFloat2(self.writeAndRead("POWER?"))

    def setCurrent(self,fValue):
        self.write("I %f"%fValue)
        pass
    
    def getSetCurrent(self):
        return self.toFloat1(self.writeAndRead("I?"))

    def getCurrent(self):
        return self.toFloat2(self.writeAndRead("IO?"))
    
    def enableOutput(self,bValue):
        self.write("OP %i" % (1 if bValue==True else 0))
        pass
    
    def lockNAM(self):
        self.write("LNA")
        pass
    
    def enableNAM(self):
        self.write("UDC")
        pass
    
    def close(self):
        self.com.close()
        pass
    
    def setVerbose(self,enable):
        self.verbose=enable
        pass
    
    def getVersion(self):
        return self.writeAndRead("*IDN?")
        
    pass
